package routes

import (
	"proje3/controllers"

	"github.com/labstack/echo/v4"
)

func Apiroutes() {
	route := echo.New()
	route.GET("/", controllers.Userscontroller)
	route.Logger.Fatal(route.Start(":9000"))
}
