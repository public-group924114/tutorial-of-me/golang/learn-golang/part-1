package models

import (
	"fmt"
	"proje3/configs"
)

type Users struct {
	ID   int
	Nama string `json:"nama" form:"nama"`
}

func WhereUser(id int) ([]Users, error) {
	var users []Users
	db := configs.ConfigDatabase()

	fmt.Println(db)
	err := db.Raw("SELECT id, name FROM users WHERE id = 1").Scan(&users).Error
	fmt.Println(err)
	if err != nil {
		return users, err
	}
	return users, nil
}
