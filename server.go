package main

import (
	"os"
	"proje3/configs"
	"proje3/routes"

	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()
	configs.ConfigDatabase()
	api := os.Getenv("API_ROUTE")
	println(api)
	if api == "api" {
		routes.Apiroutes()
	}
}
