package configs

import (
	"os"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConfigDatabase() *gorm.DB {
	configDb := os.Getenv("MYSQL_USER") + ":" + os.Getenv("MYSQL_PASS") + "@tcp(" + os.Getenv("MYSQL_HOST") + ")/" + os.Getenv("MYSQL_DATA")
	var err error
	DB, err := gorm.Open(mysql.Open(configDb))
	if err != nil {
		panic("Cannot be connected database")
	}
	DB.AutoMigrate()
	return DB
}
