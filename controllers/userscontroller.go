package controllers

import (
	"fmt"
	"net/http"
	"proje3/models"
	"strconv"

	"github.com/labstack/echo/v4"
)

type Response struct {
	ErrorCode int         `json:"error_code" form:"error_code"`
	Message   string      `json:"message" form:"message"`
	Data      interface{} `json:"data"`
}

func Userscontroller(c echo.Context) error {
	response := new(Response)
	idStr := c.QueryParam("id")
	fmt.Println(idStr)
	id, err := strconv.Atoi(idStr)
	users, err := models.WhereUser(id)
	if err != nil {
		response.ErrorCode = 10
		response.Message = "Data not found"
	} else {
		response.ErrorCode = 0
		response.Message = "Get data"
		response.Data = users
	}
	return c.JSON(http.StatusOK, response)
}
